import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import inputPass from './inputpass';

// const tahun = React.createElement(
//   'h1', {className: 'greeting'},'2020'
// );

// const list = (
//   <div>
//     <h2>Ini</h2>
//     <h2>Tahun :</h2>
//   </div>
// )

// const element2 = (<h3>Hallo {list}, {tahun}</h3>);

 
ReactDOM.render(
  // element2,
  <React.StrictMode>
    <App />
    <inputPass/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
